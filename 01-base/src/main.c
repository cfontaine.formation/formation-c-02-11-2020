#include <stdio.h> /* inclure le fichier d'en-tête stdio.h qui contient les entrée/sortie standard */

#define PI 3.14; /* définir une constante en utilisant la directive de préprocesseur #define */

/* Commentaire 
   sur plusieurs 
   lignes
*/

/* Fonction main => point d'entré du programme */
int main(void)
{
   printf("hello world");

   /* Déclaration d'une variable */
   int i;

   /* Initialisation d'une variable */
   i = 10;

   /* Déclaration Déclaration multiple de variable */
   double md, me;

   /* Déclaration multiple de variable et initialisation */
   int mi = 0, mj = 0;

   /* Littéral virgule flottante */
   double hauteur = 100.0;
   double largeur = 4e2; /* écriture exponentiel */

   /* Littéraux entier changement de base */
   int dec = 42;   /* base par défaut: décimal */
   int hex = 0x42; /* 0x-> héxdécimal */
   int oct = 017;  /* 0-> octal */
   printf("décimal=%d hexadecimal=%x octal=%o\n", dec, hex, oct);

   /* Littéral entier => par défaut de type int */
   long l = 1000L;         /* litéral long => L */
   unsigned int ui = 345U; /* litéral unsigned => U */

   /* Littéral virgule flottante => par défaut de type double */
   float f = 12.3F; /* littéral float => F */

   /* Littéral caractère */
   char chr = 'A';        /* Littéral caractère entre simple quote '' */
   char chrOctal = '\41'; /* Littéral caractère en octal -> '\ ' */
   char chrHex = '\x41';  /* Littéral caractère en hexadécimal -> '\x ' */
   printf(" %c %c %c", chr, chrOctal, chrHex);

   /* printf -> affichage dans la console */
   printf("%d\n", i); /* format -> "%d\n" %d-> correspont une variable entière, \n => au retour à la ligne*/
   int j = 42;
   printf("j= %-4d, i=%+d\n", j, i); /* */

   double d = 12.333333;
   printf("d=%4.2lf\n", d); /* */

   char c;
   int si;
   /* scanf -> saisie sur l'entrée standard (clavier) */
   scanf("%c %d", &c, &si);
   printf("c=%c  si=%d", c, si);

   fflush(stdin); /* vider le buffer d'entrée*/

   /* Afficher un seul caractère */
   putchar(chr);

   /* Saisie d'un seul  caractére */
   chr = getchar();

   /* Constante en utilisant #define */
   double var = PI;
   printf("%f\n", var);

   /* Constante en utilisant const */
   const int CST = 123; /* On est obligé d'initialiser une constante à l'initialisation*/
   printf("%d\n", CST);
   /*cst=1;  une fois la constante définit, on ne peut plus changer sa valeur */

   /* Convertion implicite => rang inférieur vers un rang supérieur (pas de perte de donnée)*/
   int cii = 12;
   double cid = cii;
   printf("%d %f\n", cii, cid);

   /* Convertion implicite avec perte de precision: long -> float*/
   long l1 = 1234567899L;
   long l2 = 1234567898L;
   float f1 = l1;
   float f2 = l2;
   long diffL = l1 - l2;
   float diffF = f1 - f2;
   printf("Perte de precision %ld %f\n", diffL, diffF); /* 1 0.00000 */

   /* Convertion explicite => opérateur de cast (type) */
   int cei1 = 5;
   int cei2 = 2;
   double ced = ((double)cei1) / cei2;
   printf("%f\n", ced);

   /* Dépassement de capacité*/
   int dep = 300;  /* 0001 0010 1100	300 => 300 > 127, la valeur maximal d'un char*/
   char cep = dep; /*      0010 1100	44 => dans cep, on n'a que le premier octet de dep */
   printf("%d %d\n", dep, cep);

   /* Opérateur arithméthique */
   int op1 = 12;
   int op2 = 30;
   int res1 = op1 + op2;
   printf("%d %d \n", res1, 3 % 2); /* % -> opérateur modulo: reste de la division entière 

    /* Opérateur d'incrémentation */
   /* Pre-incrémentation */
   i = 0;
   int res = ++i; /* i->1 res->1 */
   printf("%d %d \n", i, res);

   /* Post-incrémentation */
   i = 0;
   res = i++; /* i->1 res->0 */
   printf("%d %d \n", i, res);

   /* Affectation composée
    i+=12; /* équivalent à i=i+12 */

   /* Boolean en C valeur égale à 0 -> faux, valeur différente de 0 -> vrai */
   /* Opérateur de comparaison */
   scanf("%d", &i);

   printf("%d", i == 12);

   printf("%d", !(i == 12));

   /* Rappel logique
                        ET       OU   
         a       b     a && b   a || b
        Faux    Faux    Faux     Faux  
        Faux    Vrai    Faux     Vrai  
        Vrai    Faux    Faux     Vrai  
        Vrai    Vrai    Vrai     Vrai  
    
      Opérateur court-circuit && et || (évaluation garantie de gauche à droite)
	   && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
	   || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
    */
   i = 9;
   printf("%d", i > 12 && i < 100); // comme i >12 est faux,  i< 100 n'est pas évalué
   printf("%d", i == 12 || i == 100);

   /*
        Opérateur binaire
                         OU exclusif     
         a       b      a ^ b
        Faux    Faux    Faux   
        Faux    Vrai    Vrai  
        Vrai    Faux    Vrai  
        Vrai    Vrai    Faux    

    /* Pour les opérations sur des binaires, on utilise les entiers non signé */
   unsigned int bin = 0x13; /*  13 -> 00010011 */

   printf("%x", ~bin); /*  complémént: 1 -> 0 et 0 -> 1 */

   /* et bit à bit */
   printf("%x", bin & 2); /*  13 -> 00010011 */
                          /* & 2 -> 00000010  => 00000010 */

   /* ou bit à bit */
   printf("%x", bin | 4); /*  13 -> 00010011 */
                          /* | 4 -> 00000100  => 00010111 */

   /* ou exclusif bit à bit */
   printf("%x", bin ^ 2); /* 13  -> 00010011*/
                          /* ^ 2 -> 00000010  => 00010001 */

   /* Opérateur de décalage */
   printf("%x", bin >> 1); /* Décalage à droite de 1 00010011 => 00001001 */
   printf("%x", bin << 1); /* Décalage à gauche de 1 00010011 => 00100110 */
   printf("%x", bin << 4); /* Décalage à gauche de 4 00010011 => 10110000 */

   return 0; // Si le code, c'est exécuté normalement, la fonction main doit retourner 0
}