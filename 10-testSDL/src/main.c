#include <SDL2/SDL.h>
/**
 * @file main.c
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2020-07-03
 * 
 * @copyright Copyright (c) 2020
 * 
 */

int main(int argc,char* argv[])
{
    if(SDL_Init(SDL_INIT_VIDEO)<0)
    {
        return EXIT_FAILURE;
    }
    SDL_Window* pWindow= SDL_CreateWindow("Test SDL",200,200,600,400,SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN);
    if(pWindow == NULL)
    {
      SDL_Quit();  
      return EXIT_FAILURE;
    }
    SDL_Renderer* pRenderer=SDL_CreateRenderer(pWindow,-1,SDL_RENDERER_ACCELERATED);
    if(pRenderer ==NULL)
    {
        SDL_DestroyWindow(pWindow);
        SDL_Quit();  
        return EXIT_FAILURE;
    }

    SDL_Event events;
    SDL_bool run=SDL_TRUE;
    while(run)
    {
        while(SDL_PollEvent(&events))
        {
            switch(events.type){
                case SDL_WINDOWEVENT:
                if(events.window.event==SDL_WINDOWEVENT_CLOSE)
                {
                    run =SDL_FALSE;
                }
                break;
                case SDL_KEYDOWN:
                    SDL_Log("evénement keydown");
                break;
            }
        }
        SDL_SetRenderDrawColor(pRenderer,0,0,0,255);
        SDL_RenderClear(pRenderer);
        SDL_RenderPresent(pRenderer);
    }

    SDL_DestroyRenderer(pRenderer);
    SDL_DestroyWindow(pWindow);
    SDL_Quit();
    return EXIT_SUCCESS;
}