#include <stdio.h>
#include "compteBancaire.h"

#define TEST_MAXIMUM(x,y) x>y?x:y

void ecritureTexte(char *path);
void lectureTexte(char *path);
void ecritureTexteFormat(char *path);
void lectureTexteFormat(char *path);
void ecritureBin(char *path, struct compteBancaire cb[], int size);
void lectureBin(char *path, struct compteBancaire cb[], int size);
void testDeplacement(char *path);

int main(void)
{
    /*     ecritureTexte("c:/Formations/c/TestIO/test.txt");
    lectureTexte("c:/Formations/c/TestIO/test.txt"); 
    ecritureTexteFormat("c:/Formations/c/TestIO/test2.txt");
    lectureTexteFormat("c:/Formations/c/TestIO/test2.txt");*/
    /*     struct compteBancaire cs[]={{100.0,"fr5962-4","John Doe"},{500.0,"fr5962-6","Alan Smithee"},{20.0,"fr5962-5","Antoine Beretto"}};
    ecritureBin("c:/Formations/c/TestIO/cb.bin",cs,3);  
    struct compteBancaire cb[3];
    lectureBin("c:/Formations/c/TestIO/cb.bin", cb, 3);
    int i = 0;
    for (i = 0; i < 3; i++)
    {
        afficher(&cb[i]);
    }
    testDeplacement("c:/Formations/c/TestIO/cb.bin");*/
    rename("c:/Formations/c/TestIO/arenommer.txt","c:/Formations/c/TestIO/vide.txt");
    remove("c:/Formations/c/TestIO/aeffacer.txt");
    printf("maximum=%d\n",TEST_MAXIMUM(12,6));
    #if __WIN32__
        printf("%s %d %s",__FILE__,__LINE__,__DATE__);
    #else
        printf("%s %s",__FILE__,__TIME__);
    #endif
    return 0;
}

void ecritureTexte(char *path)
{
    FILE *f = fopen(path, "wt");
    if (f != NULL)
    {
        int i = 0;
        for (i = 0; i < 10; i++)
        {
            fputs("Hello World!\n", f);
        }
        fclose(f);
    }
}

void lectureTexte(char *path)
{
    FILE *f = fopen(path, "rt");
    if (f != NULL)
    {
        char str[128];
        while (!feof(f))
        {
            if (fgets(str, 128, f) != NULL)
            {
                printf("%s", str);
            }
        }
        fclose(f);
    }
}

void ecritureTexteFormat(char *path)
{
    FILE *f = fopen(path, "wt");
    if (f != NULL)
    {
        int i = 0;
        for (i = 0; i < 10; i++)
        {
            fprintf(f, "%d %s\n", i, "Hello World!");
        }
        fclose(f);
    }
}

void lectureTexteFormat(char *path)
{
    FILE *f = fopen(path, "rt");
    if (f != NULL)
    {
        int nbLigne;
        char str1[128];
        char str2[128];
        while (!feof(f))
        {
            if (fscanf(f, "%d %s %s\n", &nbLigne, str1, str2) > 0)
            {
                printf("%d %s %s\n", nbLigne, str1, str2);
            }
        }
        fclose(f);
    }
}

void ecritureBin(char *path, struct compteBancaire cb[], int size)
{
    FILE *f = fopen(path, "wb");
    if (f != NULL)
    {
        fwrite(cb, sizeof(struct compteBancaire), size, f);
    }
    fclose(f);
}

void lectureBin(char *path, struct compteBancaire cb[], int size)
{
    FILE *f = fopen(path, "rb");
    if (f != NULL)
    {
        fread(cb, sizeof(struct compteBancaire), size, f);
    }
    fclose(f);
}

void testDeplacement(char *path)
{
    struct compteBancaire cb;
    FILE *f = fopen(path, "r+b");
    if (f != NULL)
    {
      /*   fread(&cb, sizeof(struct compteBancaire), 1, f);
        afficher(&cb); */
        fseek(f, 2*sizeof(struct compteBancaire), SEEK_SET);
        fread(&cb, sizeof(struct compteBancaire), 1, f);
        afficher(&cb);
    }
    fclose(f);
}