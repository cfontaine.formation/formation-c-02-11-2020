int *genererTab(int size, int valeur);
int *saisirTab(int size);
void afficherTab(int tab[], int size);
int *redimTab(int tab[], int oldSize, int newSize);
int superieur(int a, int b);
int inferieur(int a, int b);
void trierTab(int tab[], int size, int (*comparaison)(int, int));