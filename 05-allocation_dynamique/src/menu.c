#include "menu.h"
#include "tableau.h"
#include <stdio.h>
#include <stdlib.h>

void afficherMenu()
{
    printf("1 - générer un tableau\n");
    printf("2 - saisir un tableau\n");
    printf("3 - afficher un tab\n");
    printf("4 - trier un tableau dans l'ordre croissant\n");
    printf("5 - trier un tableau dans l'ordre décroissant\n");
    printf("6 - redimensionner le tableau\n");
    printf("0 - quitter\n");
    printf("choix=	\n");
}
int saisirMenu()
{
    int choix;
    do
    {
        scanf("%d", &choix);
    } while (choix < 0 || choix > 6);
    return choix;
}

void menu()
{
    int *tab = NULL;
    int size = 0;
    int choix;
    do
    {
        afficherMenu();
        choix = saisirMenu();
        switch (choix)
        {
        case 1:
            if (tab != NULL)
            {
                free(tab);
            }
            printf("Saisir taille= ");
            scanf("%d", &size);
            int val;
            printf("Saisir la valeur= ");
            scanf("%d", &val);
            tab = genererTab(size, val);
            break;
        case 2:
            if (tab != NULL)
            {
                free(tab);
            }
            printf("Saisir taille= ");
            scanf("%d", &size);
            tab = saisirTab(size);
            break;
        case 3:
            if (tab != NULL)
            {
                printf("%d\n",size);
                afficherTab(tab, size);
            }
            break;
        case 4:
            if (tab != NULL)
            {
                trierTab(tab, size, superieur);
            }
            break;
        case 5:
            if (tab != NULL)
            {
                trierTab(tab, size, inferieur);
            }
            break;
        case 6:
            if (tab != NULL)
            {
                int newSize;
                scanf("%d", &newSize);
                tab = redimTab(tab, size, newSize);
                size = newSize;
            }
            break;
        }
    } while (choix != 0);
}