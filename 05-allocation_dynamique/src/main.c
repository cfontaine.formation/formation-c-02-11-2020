#include <stdio.h>
#include <stdlib.h>
#include "menu.h"

double x(double v)
{
    return v;
}

double x2(double v){
    return v*v;
}

int main(void)
{
    /* Pointeur constant */
    char chr='a';
    const  char* ptr;
    ptr=&chr;
    printf("%c\n",*ptr);
  /*  *ptr='z';
    printf("%c\n",*ptr); */ 

    char* const ptr2=&chr;
   /*ptr2=NULL;*/
    printf("%c\n",*ptr2);
    *ptr2='z';
    printf("%c\n",*ptr);

    const char* const ptr3=&chr;
  /*  *ptr3='F';
  ptr3=NULL;
    return 0; */

    /* égalié entre pointeur */
    int a=123;
    int* ptr1=&a;
    int * ptr4=ptr1;

    int tabI[]={1,3,4,5,6};
    int* ptrI=tabI;

    printf("%d\n",*(ptrI+3)); /*tab[3]*/
    printf("%d\n",ptrI[3]);

    /* Sosutraction pointeur */
    int *ptrFin=ptrI+4;
    printf("%d\n",*ptrFin);
    printf("%d\n",*tabI);
    printf("%d\n",ptrFin-ptrI);
    printf("%d\n",ptrFin[-2]);

    /* Tableau à 2 dimensions*/
    int tab2d[3][2];
    int *ptr2d=(int*)tab2d;
    *ptr2d=12;
    printf("%d\n",tab2d[0][0]);
    int *ptrLigne1=((int*)tab2d)+1;
    *ptrLigne1=100;
    printf("%d\n",tab2d[0][1]);
    /* pointeur générique */
    void* ptrGen;
    double d=12.78;
    double* ptrDouble=&d; 
    ptrGen=ptrDouble;
    printf("%lf\n",*ptrDouble);
   /* printf("%lf",*ptrGen);*/

   double* p1=ptrGen;
   printf("%lf\n",*p1);

    int* ptrA1=malloc(20*sizeof(int));  /* int ptrA1[20] */ 
    if(ptrA1!=NULL)
    {
        ptrA1[0]=123;
        printf("%d\n",ptrA1[0]);
        free(ptrA1);
        ptrA1=NULL;
    }
    /* Tableau dynamique */
    /* int size;
    scanf("%d",&size);
    char* ptrChr=malloc(sizeof(char)*size);
    if(ptrChr!=NULL)
    {
        int i;
        for(i=0;i<size;i++)
        {
            ptrChr[i]='a'+(i%26);
        }
        for(i=0;i<size;i++)
        {
            printf("tab[%d]=%c",i,ptrChr[i]);
        }
        free(ptrChr);
        ptrChr=NULL;
    } */

    /* calloc */
   /*  scanf("%d",&size);
    int* ptrInt=calloc(size,sizeof(int));
    if(ptrInt!=NULL)
    {
        int i;
        for(i=0;i<size;i++)
        {
            printf("tab[%d]=%d\n",i,ptrInt[i]);
        }
        free(ptrInt);
        ptrChr=NULL;
    } */

    /* realloc*/
   /*  char* ptrRea=malloc(100);
    if(ptrRea!=NULL)
    {
       ptrRea=realloc(ptrRea,15);
       int i;
       for(i=0;i<15;i++)
        {
            ptrRea[i]='a'+(i%26);
        }
        for(i=0;i<15;i++)
        {
            printf("%c",ptrRea[i]);
        }
        free(ptrRea);
        ptrRea=NULL;
    }

    double(*ptr_fonction)(double);  
    int choix;
    scanf("%d",&choix);
    if(choix==1)
    {
        ptr_fonction= x;
    }
    else
    {
        ptr_fonction= x2;
    }
    
    
    for(double v=-10.0;v<10.0;v+=0.5)
    {
        printf("%lf\n",ptr_fonction(v));
    }  */
    menu();
}