#include "tableau.h"
#include <stdio.h>
#include <stdlib.h>

int *genererTab(int size, int valeur)
{
    int *tab = malloc(sizeof(int) * size);
    int i;
    for (i = 0; i < size; i++)
    {
        tab[i] = valeur;
    }
    return tab;
}

int *saisirTab(int size)
{
    int *tab = malloc(sizeof(int) * size);
    int i;
    for (i = 0; i < size; i++)
    {
        printf("tab[%d] = ", i);
        scanf("%d", &tab[i]);
    }
    return tab;
}

void afficherTab(int tab[], int size)
{
    int i = 0;
    printf("[ ");
    for (int i; i < size; i++)
    {
        printf("%d ", tab[i]);
    }
    printf("]\n");
}

int *redimTab(int tab[], int oldSize, int newSize)
{
    int *tmp = realloc(tab, newSize*sizeof(int));
    int i = 0;
    for (i = oldSize; i < newSize; i++)
    {
        tmp[i] = 0;
    }
    return tmp;
}

int superieur(int a, int b)
{
    return a > b;
}

int inferieur(int a, int b)
{
    return a < b;
}

void trierTab(int tab[], int size, int (*comparaison)(int, int))
{
    int ok;
    do
    {
        ok=1;
        int i;
        for (i = 0; i < size - 1; i++)
        {
            if (comparaison(tab[i], tab[i + 1]))
            {
                int tmp = tab[i];
                tab[i] = tab[i + 1];
                tab[i + 1] = tmp;
                ok = 0;
            }
        }
    } while (ok == 0);
}
