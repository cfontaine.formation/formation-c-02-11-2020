
#ifndef POINT_H
#define POINT_H 
struct point
{
    int x;
    int y;
};

void afficherPoint(struct point *p);
void initPoint(struct point *p, int x, int y);
double distance(struct point *p1, struct point *p2);
#endif