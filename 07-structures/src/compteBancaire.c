#include "compteBancaire.h"
#include <stdio.h>

void afficher(struct compteBancaire * cb)
{
    printf("___________________________________________________\n");
    printf("Solde = %lf\n",cb->solde);
    printf("Iban = %s\n",cb->iban);
    printf("Titulaire = %s\n",cb->titulaire);
    printf("___________________________________________________\n");
}

void crediter(struct compteBancaire * cb,double valeur)
{
    if(valeur>0)
    {
        cb->solde+=valeur;
    }
}

void debiter(struct compteBancaire * cb,double valeur)
{
    if(valeur>0)
    {
        cb->solde-=valeur;
    }
}

int estPositif(struct compteBancaire * cb)
{
    return cb->solde>0;
}



