#ifndef LISTE_H
#define LISTE_H
typedef struct elementList* ptrElementList;

struct elementList
{
    ptrElementList suivant;
    int data;
};

ptrElementList initList();
void ajouterElement(ptrElementList *list, int data);
void afficherListe(ptrElementList list);
void effacerListe(ptrElementList *list);
#endif
