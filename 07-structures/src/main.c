#include "point.h"
#include <stdlib.h>
#include <stdio.h>
#include "liste.h"
#include "compteBancaire.h"
#include "MesStructures.h"


int main(void)
{
    /*    struct point p1, p2;
    p1.x = 0;
    p1.y = 0;
    p2.x = 1;
    p2.y = 2;

    struct point p3 = {1, 1};

    struct point p4;
 */
    /* Affectation d'une structure*/
    /*     p4 = p2;
    printf("x=%d y=%d\n", p4.x, p4.y); */

    /* Comparaison de  2 structures*/
    /*     if (p4.x == p2.x && p4.y == p2.y)
    {
        printf("les 2 points sont égaux\n");
    } */

    /*  struct point tab[3];
    tab[0].x = 1;
    tab[0].y = 3;
    tab[1].x = 6;
    tab[1].y = 3;
    tab[2].x = 9;
    tab[2].y = 2;

    struct point tab2[] = {{1, 3}, {4, 9}};
    int i = 0;
    for (i = 0; i < 3; i++)
    { */
    /* printf("x=%d y=%d\n",tab[i].x,tab[i].y);*/
    /*    afficherPoint(&tab[i]);
    }
    printf("distance entre p1 et p3= %lf",distance(&p1,&p3)); */

    ptrElementList l = initList();
    ajouterElement(&l, 3);
    ajouterElement(&l, 4);
    ajouterElement(&l, 1);
    ajouterElement(&l, 9);
    ajouterElement(&l, 3);
    afficherListe(l);
    effacerListe(&l);

    union prime pri;
    pri.fixe=1000;
    printf("%d\n", pri.fixe);
    pri.taux=0.10;
    printf("%lf\n", pri.taux);
    printf("%lf\n", pri.fixe);
    enum direction {NORD=90,EST=180,SUD=270,OUEST=0};

    enum direction dir=SUD;
    printf("%d, ",dir);
    struct mot m;
    m.a=1;
    m.b=3;
    m.c=25;
    m.d=3;
    printf("%x %x %x \n",m.a,m.b,m.d); 

    return EXIT_SUCCESS;
}