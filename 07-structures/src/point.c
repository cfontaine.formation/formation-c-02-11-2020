#include "point.h"
#include "stdio.h"
#include <math.h>

void afficherPoint(struct point * p)
{
    printf("[%d,%d]",p->x,p->y);
}

void initPoint(struct point * p,int x, int y)
{
    p->x=x;
    p->y=y;
}

double distance(struct point * p1,struct point * p2)
{
    return sqrt(pow(p2->x-p1->x,2) + pow(p2->y-p1->y,2));
}

