#ifndef COMPTEBANCAIRE_H
#define COMPTEBANCAIRE_H
struct compteBancaire
{
    double solde;
    char iban[10];
    char titulaire[30];
};

void afficher(struct compteBancaire * cb);
void crediter(struct compteBancaire * cb,double valeur);
void debiter(struct compteBancaire * cb,double valeur);
int estPositif(struct compteBancaire * cb);
#endif