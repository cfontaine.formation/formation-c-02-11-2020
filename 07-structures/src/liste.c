#include "liste.h"
#include <stdlib.h>
#include <stdio.h>


ptrElementList initList()
{
    return NULL;
}
void ajouterElement(ptrElementList *list, int data)
{
    ptrElementList elm=malloc(sizeof(struct elementList));
    elm->data=data;
    elm->suivant=*list;
    *list=elm;
}
void afficherListe(ptrElementList list)
{
    ptrElementList ptrElm=list;
    while (ptrElm!=NULL)
    {
        printf("%d\n",ptrElm->data);
        ptrElm=ptrElm->suivant;
    }
}

void effacerListe(ptrElementList *list)
{
    ptrElementList ptrElm=*list;
    ptrElementList ptrDel;
    while (ptrElm!=NULL)
    {
            ptrDel=ptrElm;
            ptrElm=ptrElm->suivant;
            free(ptrDel);
    }
    *list=NULL;
}