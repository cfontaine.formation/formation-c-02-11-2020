#include <stdio.h>

#define EPS 0.00001

int main(void)
{
    int val;
    printf("Saisir une valeur entiere: ");
    scanf("%d", &val);

    /* Conditions __________________________________
        if : si
        else : sinon
    */
    if (val > 10)
    {
        printf("val est supérieur à 10\n");
    }
    else
    {
        printf("val est inférieur ou égal à 10\n");
    }

    /* else n’est pas obligatoire */
    if (val > 15)
    {
        printf("val est supérieur à 15\n");
    }

    /* On peut imbriquer les if / else */
    if (val == 12)
    {
        printf("val est égal à 12\n");
    }
    else if (val == 13)
    {
        printf("val est égal à 13\n");
    }
    else
    {
        printf("val vaut une autre valeur\n");
    }

    /* Exercice : Somme
        Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 + 3 = 4 */
    int v1, v2;
    printf("Saisir 2 entiers: ");
    scanf("%d %d", &v1, &v2);
    int somme = v1 + v2;
    printf("%d + %d = %d\n", v1, v2, somme);

    /* Exercice : Moyenne 
       Saisir 2 nombres entiers et afficher la moyenne dans la console */
    int a, b;
    printf("Saisir 2 entiers: ");
    scanf("%d %d", &a, &b);
    double moyenne = (a + b) / 2.0;
    printf("moyenne=%f\n", moyenne);

    /* Exercice :Parité 
       Créer un programme qui indique, si le nombre entier saisie dans la console est paire ou impaire
    */
    int v;
    printf("Saisir un entier: ");
    scanf("%d", &v);
    if (v % 2 == 0)
    {
        printf("Le nombre %d est paire\n", v);
    }
    else
    {
        printf("Le nombre %d est impaire\n", v);
    }

    /* Exercice maximum
       Saisir 2 nombres et afficher le maximum entre ces 2 nombres
    */
    printf("Saisir 2 nombres entiers :");
    int val1, val2;
    scanf("%d %d", &val1, &val2);
    if (val1 > val2)
    {
        printf("%d est le maximum\n", val1);
    }
    else
    {
        printf("%d est le maximum\n", val2);
    }

    /* Exercice intervalle 
       Saisir un nombre et dire s'il fait parti de l'intervalle -4 (exclus) et 7 (inclus) */
    printf("Saisir un nombre : ");
    int num;
    scanf("%d", &num);
    if (num > -4 && num <= 7)
    {
        printf("Le nombre %d fait partie de l'intervalle -4 à 7\n", num);
    }

    /* Condition Switch */
    int jours;
    printf("Enter une valeur d'un jour");
    scanf("%d", &jours);
    switch (jours)
    {
    case 1:
        printf("Lundi\n");
        break;
    case 6:
    case 7:
        printf("Week end !\n");
        break;
    default:
        printf("Un autre jour\n");
    }

    /* Exercice: Nombre de jour
       Saisir le mois (de 1 à 12) et afficher le nombre de jour de ce mois: 30,31,ou 28/29 */
    int mois;
    printf("Entrer une valeur d'un mois (1 à 12)");
    scanf("%d", &mois);
    switch (mois)
    {
    case 2:
        printf("28/29\n");
        break;
    case 4:
    case 6:
    case 9:
    case 11:
        printf("30\n");
        break;
    default:
        printf("31\n");
    }

    /* Exercice calculatrice
        Saisir dans la console 
       -   un double
       -   une caractère opérateur qui a pour valeur valide : __+ - * /__
       -   un double

       Afficher:
        -   Le résultat de l’opération
        -   Une message d’erreur si l’opérateur est incorrecte   
        -   Une message d’erreur si l’on fait une division par 0 */
    double d1, d2;
    char operateur;
    printf("Saisir un nombre un opérateur et un deuxième nombre : ");
    scanf("%lf %c %lf", &d1, &operateur, &d2);
    switch (operateur)
    {
    case '+':
        printf("= %f\n", d1 + d2);
        break;
    case '-':
        printf("= %f\n", d1 - d2);
        break;
    case '*':
        printf("= %f\n", d1 * d2);
        break;
    case '/':
        /* ( d2 > -EPS && d2 < EPS) après un calcul il ne vaut pas faire des tests d'égalités
        mais tester un intervalle pour prendre en compte les erreurs de calculs float , double, long double
        sont des valeurs approchées 
        Ici, comme c'est une valeur saisie, on peut tester l'égalité */
        if (d2 == 0.0)

        {
            printf("division par 0");
        }
        else
        {
            printf("= %f", d1 / d2);
        }
        break;
    default:
        printf("l'opérateur %c n'est pas valide", operateur);
    }
    int n;

    /* Opérateur ternaire  => affectation conditionnelle */
    scanf("%d", &n);
    printf("%s", n % 2 == 0 ? "Nombre paire" : "Nombre impaire");

    /* Boucle while*/
    int i = 0;
    while (i < 3)
    {
        printf("%d\n", i);
        i++;
    }

    /* Boucle for */
    for (i = 0; i < 10; i++)
    {
        printf("i=%d\n", i);
    }

    /* Exercice : Table de multiplication
       Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
	    1 X 4 = 4
		2 X 4 = 8
		…
		9 x 4 = 36
		Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9, on ne fait rien */
    int mult;
    printf("Saisir une valeur de 1 à 9 ");
    scanf("%d", &mult);
    if (mult >= 1 && mult <= 9)
    {
        int i;
        for (i = 0; i <= 9; i++)
        {
            printf("%d x %d = %d", i, mult, i * mult);
        }
    }

    /* Exercice quadrillage 
        Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne  
        ex: pour 2 3  
        
        [ ][ ]  
        [ ][ ]  
        [ ][ ] 
    */
    int ligne, colonne;
    printf("Entrer le nombre de colonne et de ligne : ");
    scanf("%d %d", &colonne, &ligne);
    int l;
    int c;
    for (l = 0; l < ligne; l++)
    {
        for (c = 0; c < colonne; c++)
        {
            printf("[ ] ");
        }
        printf("\n");
    }

    /* Instructions de branchement
	    break */
    for (i = 0; i < 5; i++)
    {
        printf("i=%d\n", i);
        if (i == 3)
        {
            break; /* break => termine la boucle */
        }
    }

    i = 0;
    while (1) /* boucle infinie */
    {
        if (i == 3)
        {
            break; /* break permet de quitter la boucle */
        }
        printf("i=%d\n", i);
        i++;
    }

    /* continue */
    for (i = 0; i < 10; i++)
    {
        if (i == 3)
        {
            continue; /* continue => on passe à l'itération suivante */
        }
        printf("i=%d\n", i);
    }

    /* goto */
    int j;
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 10; j++)
        {
            printf("i=%d j=%d\n", i, j);
            if (i == 3)
            {
                goto EXIT_LOOP; /* Utilisation de goto pour sortir de 2 boucles imbriquées */
            }
        }
    }

EXIT_LOOP:
    printf("/n");

    /* Pointeur */
    /* Déclaration d'un pointeur d'entier et initialisation à NULL */
    int *ptr = NULL;
    /* NULL -> Un pointeur qui ne pointe sur rien */
    printf("%p\n", ptr);
    int x = 1;
    printf("%p\n", &x); /* &x => adresse de la variable x */
    ptr = &x;
    printf("%p\n", ptr);
    printf("%d\n", *ptr); /* *ptr => Accès au contenu de la variable pointée par ptr */
    *ptr = 30;            /*  modification de la variable x par l'intermédiaire du pointeur ptr */
    printf("%d\n", *ptr);
    printf("%d\n", x);

    /* Pointeur
        Pour 2 variables va=12.5 et vb=30.7
        - créer 2 pointeurs ptra un poiteur sur a et ptrb un pointeur sur b
        - avec les pointeurs  
                - afficher l'adresse de la variable b
                - afficher la somme de a et b
	            - permuter la valeur de a et de b */
    double va = 12.5;
    double vb = 30.7;
    double *ptrVa;
    ptrVa = &va;

    double *ptrVb = &vb;

    printf("%p", ptrVb);
    printf("%lf", *ptrVa + *ptrVb);

    double tmp = *ptrVa;
    *ptrVa = *ptrVb;
    *ptrVb = tmp;
    printf("a=%lf b=%lf", *ptrVa, *ptrVb);

    /* Opérateur sizeof: Renvoie la taille en octet d’une variable ou d’un type  */
    printf("%d\n", sizeof(a));
    printf("%d\n", sizeof(ptrVa));
    printf("%d\n", sizeof(char));

    /* Opérateur séquentiel */
    double d = 23.98;
    double resultat = (a * b, a + d); /* a*b est évalué, mais c'est a+d qui est affecté à résultat. l'execution se fait de gauche à droite */

    /* Promotion numérique */
    int pri = 11;
    double pr2 = 30.0;
    double prRes = pri + pr2; /* pri va être convertie en double avant de faire l'addition */
    prRes = pri / 2.0;

    short prs1 = 15;
    short prs2 = 45;
    int prsi = prs1 + prs2; /* prs1 et prs2 vont être converti vers un entier avant d'être ajouté */

    return 0;
}