#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

char *inverser(char *str);

int main(void)
{
    /*Chaine constante*/
    char *str1 = "Hello World!";
    printf("%s\n", str1);
    /*   str1[0]='h';*/

    char str[] = "hello World"; /*hello World\0*/
    str[0] = 'H';
    printf("%s (%d)\n", str, sizeof(str));

    /* strlen => longueur de la chaine*/
    printf("%d\n", strlen(str));

    /* concaténation de 2 chaines */
    strcat(str, " !!!!!!!!!!!!!!!");
    printf("%s\n", str);

    strncat(str, " !!!!!!!!!!!!!!!", 2);
    printf("%s\n", str);

    /* Comparaison de chaine */
    char str2[] = "hello_____World!";
    char str3[] = "hello World!";
    printf("%d\n", strcmp(str2, str3));
    printf("%d\n", strncmp(str2, str3, 5));

    /* Copie d'une chaine */
    char str4[17];
    strcpy(str4, str2);
    printf("%s\n", str4);
    strncpy(str4, str2, 5);
    str4[5] = '\0';
    printf("%s\n", str4);

    /* rechercher un caractère dans une chaine */
    char *p = strchr(str3, 'o');
    printf("position dans la chaine  de o=%d\n", p - str3);

    char *p2 = strrchr(str3, 'o');
    printf("position dans la chaine  de o=%d\n", p2 - str3);

    /* rechercher un sous-chaine dans une chaine */
    char *pstr = strstr(str3, "World");
    printf("position dans la chaine  de World=%d\n", pstr - str3);

    char *s = inverser("bonjour");
    printf("%s", s);
    free(s);

    printf("%d\n", atoi("1234"));
    printf("%d\n", atoi("12ffg"));
    printf("%d\n", atoi("bonjour"));

    printf("%ld\n", atol("1234"));
    printf("%lf\n", atof("12.34"));

    printf("%d\n", isalpha('a'));
    printf("%d\n", isalpha('1'));

    printf("%d\n", isupper('a'));
    printf("%d\n", isupper('A'));
    printf("%c\n", tolower('A'));

    printf("%lf\n", pow(3.0, 2.0));
    printf("%lf\n", sqrt(9.0));
    printf("%lf\n", cos(3.14 / 2));
    printf("%lf\n", sin(0.0));
    printf("%lf\n", ceil(1.4));
    printf("%lf\n", floor(1.4));
    printf("%lf\n", round(1.4));
    printf("%lf\n", round(1.6));
    printf("%lf\n", trunc(1.4));

    return 0;
}

char *inverser(char *str)
{
    int l = strlen(str);
    char *ptr = malloc(l + 1);
    int i;
    for (i = 0; i < l; i++)
    {
        ptr[i] = str[l - i - 1];
    }
    ptr[l] = '\0';
    return ptr;
}

int palindrome(char *str)
{
    char *strInv = inverser(str);
    if (strcmp(str, strInv) == 0)
    {
        free(strInv);
        return 1;
    }
    free(strInv);
    return 0;
}