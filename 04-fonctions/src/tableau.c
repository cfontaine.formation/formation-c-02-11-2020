#include "tableau.h"
#include <stdio.h>

/*void fonctionTableau(int* tab,int size)*/
void fonctionTableau(int tab[], int size)
{
    if (size > 1)
    {
        printf("%d\n", tab[0]);
    }
    tab[0] = 100;
}

int estPresent(char tab[], int size, char c)
{
    int i;
    for (i = 0; i < size; i++)
    {
        if (tab[i] == c)
        {
            return 1;
        }
    }
    return 0;
}

void afficherTab(int tab[], int size)
{
    int i;
    printf("[ ");
    for (i = 0; i < size; i++)
    {
        printf("%d ", tab[i]);
    }
    printf("]\n");
}

void saisirTab(int tab[], int *size)
{
    printf("Saisir la taille du tableau :");
    scanf("%d", size);
    if (*size < 20)
    {
        int i;
        for (i = 0; i < *size; i++)
        {
            printf("Tab[%d]=", i);
            scanf("%d", &tab[i]);
        }
    }
}

void calculTab(int tab[], int size, int *maximum, double *moyenne)
{
    *maximum = tab[0];
    double somme = 0;
    int i;
    for (i = 0; i < size; i++)
    {
        if (tab[i] > *maximum)
        {
            *maximum = tab[i];
        }
        somme += tab[i];
    }
    *moyenne = somme / size;
}
