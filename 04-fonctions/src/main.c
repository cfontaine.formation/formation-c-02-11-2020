#include <stdio.h>
#include <stdlib.h>
#include "tableau.h"
#include "classeMemorisation.h"

/*Déclaration des fonctions*/
int somme(int v1, int v2);
void afficher(int v);
int even(int v);
int maximum(int a, int b);
void permuter(int *a, int *b);
int factorial(int n);
/* void fonctionTableau(int [], int ); */

/* Variable globale */
int varGlobale=100;
static double varGlobaleStatic;

int main(int argc, char* argv[])
{
    int add = somme(21, 35);
    afficher(add);
    printf("parité=%d\n", even(2));
    printf("parité=%d\n", even(3));
    printf("maximum=%d\n", maximum(10, 20));
    printf("maximum=%d\n", maximum(30, 5));

    /* exit(0); permet de quitter le programme */
    int v1 = 2;
    int v2 = 5;
    permuter(&v1, &v2);
    printf("v1=%d v2=%d\n", v1, v2);

    int t[] = {3, 7, 5, 4};
    fonctionTableau(t, 4);
    printf("%d\n", t[0]);

    int resultat=factorial(3);
    printf("%d",resultat);
    int i;
    for(i=0;i<argc;i++)
    {
     /*    v1=4;
       int tttttt;
         tttttt=1; */
        printf("%s\n",argv[i]);
    }
  /*   tttttt=2; */


    printf("%d\n",varGlobale);
    int varGlobale=-3;
    printf("%d\n",varGlobale);

    /*  */
    char tc[]={'a','z','e','r','t','y'};
    printf("%d\n",estPresent(tc,6,'a'));
    printf("%d\n",estPresent(tc,6,'h'));

/*     int tab[]={2,6,3,9};
    afficherTab(tab,4); */
/*     int tab[20];
    int size;
    saisirTab(tab,&size);
    afficherTab(tab,size);
    int maximum;
    double moyenne;
    calculTab(tab,size,&maximum,&moyenne);
    printf("maximum=%d moyenne=%lf",maximum,moyenne); */

    testStatic();
    testStatic();
    testStatic();
    testRegister();
    testExtern();
    testExternLocale();
    printf("%d",varGlobaleStatic);
    testGlobalStatic();
    return 0;
}

/*Définition des fonctions*/
int somme(int v1, int v2)
{
    int res = v1 + v2;
    return res;
}

void afficher(int v)
{
    printf("%d\n", v);
}

int even(int v)
{
    return v % 2 == 0;

    /*     if(v%2==0)
    {
        return 1;
    }
    else
    {
        return 0;
    } */

    /*return v % 2 == 0?1:0;*/
}

int maximum(int a, int b)
{
    if (a > b)
    {
        return a;
    }
    else
    {
        return b;
    }
}

/* void permuter(int a, int b)
{
    printf("a=%d b=%d\n",a,b);
    int tmp=a;
    a=b;
    b=tmp;
    printf("a=%d b=%d\n",a,b);
} */

void permuter(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}



int factorial(int n) /* factoriel= 1* 2* … n */
{
    if (n <= 1) /* condition de sortie */
    {
        return 1;
    }
    else
    {
        return factorial(n - 1) * n;
    }
}

/*void fonctionTableau(int* tab,int size)*/
/* void fonctionTableau(int tab[], int size)
{
    if (size > 1)
    {
        printf("%d\n", tab[0]);
    }
    tab[0] = 100;
} */

