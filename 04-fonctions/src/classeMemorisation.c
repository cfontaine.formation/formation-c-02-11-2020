#include "classeMemorisation.h"
#include <stdio.h>

/*extern int varGlobale;*/
/*extern double  varGlobaleStatic;*/
void testStatic(void)
{
    static int val;
    printf("%d\n",val);
    val++;
}

void testRegister(void){
    register int i;
    for ( i = 0; i < 100000; i++)
    {
        int res=1+2;
    }
}

void testExtern(void)
{
    /*printf("%d\n",varGlobale);*/
}

void testExternLocale(void)
{
    extern int varGlobale;
    printf("%d\n",varGlobale);
}
void testGlobalStatic(void)
{
    /*varGlobaleStatic=1200*/;
}

