#include <stdio.h>

int main(void)
{
    /* Tableau à une dimmension

    Déclaration: type nom[taille]  */
    char tabChr[5];

    /* Initialisation des éléments du tableau */
    tabChr[0] = 'h'; /* Accès à un élément du tableau */
    tabChr[1] = 'e';
    tabChr[2] = 'l';
    tabChr[3] = 'l';
    tabChr[4] = 'o';

    int i;
    /* Parcourir un tableau à une dimmension */
    for (i = 0; i < 5; i++)
    {
        printf("tabChr[%d]=%c\n", i, tabChr[i]);
    }

    /* Déclaration et initialisation d'un tableau */
    double tab[] = {1.0, 5.5, 9.6, 3.6};
    for (i = 0; i < 4; i++)
    {
        printf("tab[%d]=%lf\n", i, tab[i]);
    }

    /* Exercice : tableau
	   Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers: -7, 4, 8, 0, -3 */
    int t[] = {-7, 4, 8, 0 - 3};
    int maximum = t[0];
    double somme = 0;
    for (i = 0; i < 5; i++)
    {
        if (t[i] > maximum)
        {
            maximum = t[i];
        }
        somme += t[i];
    }
    printf("maximum=%d moyenne=%lf\n", maximum, somme / 5);

    /* Tableau à 2 dimmensions */

    int tab2d[3][4]; /* Déclaration */

    /* Initialisation des éléments du tableau à 0*/
    int j;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            tab2d[i][j] = 0;
        }
    }

    tab2d[1][1] = 1; /* Accès à un élément du tableau */

    /* Parcourir un tableau à deux dimmensions: */
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            printf("[%d] ", tab2d[i][j]);
        }
        printf("\n");
    }
    return 0;
}