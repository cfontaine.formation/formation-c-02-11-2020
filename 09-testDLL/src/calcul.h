#ifndef CALCUL_H
#define CALCUL_H
#ifdef EXPORT
    #define DLL_FUNCTION __declspec(dllexport)
#else
    #define DLL_FUNCTION __declspec(dllimport)
#endif
int DLL_FUNCTION addition(int a, int b);
int DLL_FUNCTION multiplication(int a, int b);
#endif